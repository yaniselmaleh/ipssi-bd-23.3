// produits.jsx
import React from 'react';
import produitsData from '../../data/produits.json';
import { Link } from 'react-router-dom';

const Produits = () => {
  return (
    <div style={{ width: '80%', margin: 'auto' }}>
      <h2>Liste des produits</h2>
      {console.log(produitsData)}

      {produitsData.map((produit, index) => (
        <div key={index}>
          <h3>
            <Link to={`/produit/${produit.id}`}>{produit.nom}</Link>
          </h3>
          <p>{produit.description}</p>
          <p>Prix : {produit.prix} €</p>
          <p>Couleurs disponibles : {produit.couleurs.join(', ')}</p>
        </div>
      ))}
    </div>
  );
};

export default Produits;
