import React from 'react';
import { useParams } from 'react-router-dom';
import produitsData from '../../data/produits.json';

const Produit = () => {
  const { id } = useParams();
  const produit = produitsData.find((produit) => produit.id.toString() === id);

  if (!produit) {
    return <div>Produit introuvable</div>;
  }

  return (
    <div>
      <h2>{produit.nom}</h2>
      <p>{produit.description}</p>
      <p>Prix : {produit.prix} €</p>
      <p>Couleurs disponibles : {produit.couleurs.join(', ')}</p>
      <p>
        Dimensions : {produit.dimensions.hauteur}cm x{' '}
        {produit.dimensions.largeur}cm x {produit.dimensions.profondeur}cm
      </p>
      <p>
        Caractéristiques :
        <ul>
          <li>Poids : {produit.caracteristiques.poids} kg</li>
          <li>Matériau : {produit.caracteristiques.materiau}</li>
          <li>Fabricant : {produit.caracteristiques.fabricant}</li>
        </ul>
      </p>
      <h3>Commentaires :</h3>
      {produit.commentaires.map((commentaire, index) => (
        <div key={index}>
          <p>Auteur : {commentaire.auteur}</p>
          <p>Commentaire : {commentaire.texte}</p>
        </div>
      ))}
    </div>
  );
};

export default Produit;
