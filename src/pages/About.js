import React, { useState } from 'react';

const About = () => {
  /* `const [count, setCount] = useState(0)` is using the `useState` hook in React to declare a state
    variable called `count` and its corresponding setter function `setCount`. The initial value of
    `count` is set to 0. */
  const [count, setCount] = useState(0);

  /**
   * The function `increment` increases the value of `count` by 1.
   */
  const increment = () => {
    setCount(count + 1);
  };

  return (
    <>
      <h1>Page About</h1>
      <p>Compteur : {count}</p>
      <button onClick={increment}>Incrémenter</button>
    </>
  );
};

export default About;
