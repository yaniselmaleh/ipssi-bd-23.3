import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Main from '../pages/Main';
import About from '../pages/About';
import NoMatch from './NoMatch';
import Navigation from '../layout/navigation';
import Produits from '../component/produits/Produits';
import Produit from '../component/produits/Produit';

const Routeur = () => {
  return (
    <BrowserRouter>
      <Navigation>
        <Routes>
          <Route path="/" element={<Main />} />
          <Route path="/about" element={<About />} />
          <Route path="/produits" element={<Produits />} />
          <Route path="/produit/:id" element={<Produit />} />
          <Route path="*" element={<NoMatch />} />
        </Routes>
      </Navigation>
    </BrowserRouter>
  );
};

export default Routeur;
